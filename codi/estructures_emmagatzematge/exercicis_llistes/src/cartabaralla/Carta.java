package cartabaralla;

public class Carta {
	public static final int OROS = 0;
	public static final int COPES = 1;
	public static final int ESPASES = 2;
	public static final int BASTOS = 3;
	
	private int num;
	private int pal;
	
	public Carta(int num, int pal) throws IllegalArgumentException {
		if (num < 1 || num > 12 || pal < OROS || pal > BASTOS)
			throw new IllegalArgumentException("Carta incorrecta");
		this.num = num;
		this.pal = pal;
	}
	
	public int getNum() {
		return num;
	}
	
	public int getPal() {
		return pal;
	}
	
	@Override
	public String toString() {
		String s = Integer.toString(num);
		
		switch (pal) {
		case OROS: s+="O"; break;
		case COPES: s+="C"; break;
		case ESPASES: s+="E"; break;
		case BASTOS: s+="B"; break;
		}
		
		return s;
	}
	
}
