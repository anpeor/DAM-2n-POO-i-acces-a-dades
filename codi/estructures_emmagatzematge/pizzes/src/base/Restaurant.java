package base;

import java.util.Scanner;

import ingredients.Formatge;
import ingredients.Tomaquet;
import ingredients.Tonyina;

public class Restaurant {

	/**
	 * @param args
	 * @throws CloneNotSupportedException
	 *             No capturem aquesta excepció perquè si peta és error de
	 *             programació
	 */
	public static void main(String[] args) throws CloneNotSupportedException {
		Scanner scanner = new Scanner(System.in);
		String comanda = "";

		// Registra pizzes
		PizzaFactory pizzeria = new PizzaFactory();
		pizzeria.addPizza("Margarita", new Formatge(new Tomaquet(new BaseFina())));
		pizzeria.addPizza("Marinera", new Formatge(new Tonyina(new BaseFina())));

		// Demana pizza
		Ingredient pizza;
		System.out.println("Benvingut a la pizzeria Luigi!");
		while (!comanda.equalsIgnoreCase("n")) {
			System.out.println("\nAquestes són les nostres pizzes: ");
			for (String nom : pizzeria.getNomsPizzes())
				System.out.println(nom + " ");
			System.out.println("\nQuina pizza voleu? ");
			comanda = scanner.nextLine();
			try {
				pizza = pizzeria.getPizzaByName(comanda);
				System.out.println("El preu de la comanda és de " + pizza.getPreu());
				System.out.println("Els ingredients de la pizza són: " + pizza.descripcio());
				System.out.println("Voleu continuar (s per acceptar)?");
				comanda = scanner.nextLine();
				if (comanda.equalsIgnoreCase("s")) {
					System.out.println(pizza.recepta());
					System.out.println("Aquí té la seva pizza!");
				}
			} catch (IllegalArgumentException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("\nVoleu fer una altra comanda (n per sortir)?");
			comanda = scanner.nextLine();
		}
		System.out.println("Fins la propera!");
		scanner.close();
	}

}
