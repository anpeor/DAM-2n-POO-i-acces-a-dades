package interficies_ex3;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Tirada implements Comparable<Tirada>, Cloneable {

	private int atac;
	private int defensa;
	private int quantsDaus;
	private Integer[] daus;
	
	public Tirada() {
		this(3, 3, 5);
	}
	
	public Tirada(int atac, int defensa, int quantsDaus) {
		this.atac = atac;
		this.defensa = defensa;
		this.quantsDaus = quantsDaus;
		this.daus = new Integer[quantsDaus];
		this.generarDaus();
	}
	
	public int getAtac() {
		return atac;
	}
	
	public void setAtac(int atac) {
		this.atac = atac;
	}
	
	public int getDefensa() {
		return defensa;
	}
	
	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}
	
	public Integer[] getDaus() {
		return daus;
	}
	
	public void setDaus(Integer[] daus) {
		this.daus = daus;
	}
	
	public void generarDaus() {
		
		Random rn = new Random();
		
		for (int i=0; i<this.quantsDaus; i++) {
			this.daus[i] = rn.nextInt(6)+1;
		}
		
		Arrays.sort(this.daus, Collections.reverseOrder());
	}
	
	@Override
	public int compareTo(Tirada t) {
		int resultat = 0;
		for (int i=0; i<quantsDaus; i++) {
			resultat += ((atac + daus[i]) > (t.getDefensa() + (i >= t.getDaus().length ? 0 : t.getDaus()[i])) ? 1 : 0);
		}
		return resultat;
	}
	
	@Override
	public String toString() {
		return String.format("AT: %d DEF: %d Daus (%d): %s", atac, defensa, quantsDaus, Arrays.toString(daus));
	}
	
	@Override
	public Tirada clone() {
		try {
			return (Tirada) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
}
