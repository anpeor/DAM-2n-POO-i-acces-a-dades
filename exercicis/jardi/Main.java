package jardi;

public class Main {

	public static void main(String[] args) {
		
		Jardi jardi = new Jardi(20);
		
		jardi.plantaLlavor(new Llavor(new Altibus()), 5);
		jardi.plantaLlavor(new Llavor(new Altibus()), 7);
		jardi.plantaLlavor(new Llavor(new Declinus()), 2);
		jardi.plantaLlavor(new Llavor(new Declinus()), 8);
		
		for(int i=0; i<50; i++) {
			
			System.out.print(jardi);
			jardi.temps();
		}
	}
}
