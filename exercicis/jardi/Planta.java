package jardi;

import java.util.Random;

public class Planta {

	boolean esViva = true;
	int altura = 0;
	
	public char getChar(int nivell) {
		return ' ';
	}
	
	public Planta creix() {	
		
		altura++;
		esViva = altura >= 10 ? false : true; 
		
		return null;
	}
	
	public int escampaLlavor() {
		int[] op = {-2, -1, 1, 2};
		Random rn = new Random();
		return op[rn.nextInt(4)];
	}
	
	public int getAltura() {
		return altura;
	}
	
	public boolean esViva() {
		return esViva;
	}
}
