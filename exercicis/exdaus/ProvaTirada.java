package interficies_ex3;

public class ProvaTirada {

	public static void main(String[] args) {
		
		Tirada tiradaJ1 = new Tirada(3, 2, 4);
		Tirada tiradaJ2 = new Tirada(4, 3, 3);
						
		System.out.println(tiradaJ1.toString());
		System.out.println(tiradaJ2.toString());
				
		System.out.println("Comparació de les dues tirades: "+(tiradaJ1.compareTo(tiradaJ2)-tiradaJ2.compareTo(tiradaJ1)));
		
		Tirada tiradaJ3 = tiradaJ1.clone();
		
		System.out.println("Comparació d'una tirada amb una igual: "+(tiradaJ1.compareTo(tiradaJ3)-tiradaJ3.compareTo(tiradaJ1)));
	}

}
