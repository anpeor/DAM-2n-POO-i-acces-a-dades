package fungus;

public class Compactus extends Fungus {
	private final static int[] DIF_FILA = {-1, 0, 1, -1, 0, 1, -1, 0, 1};
	private final static int[] DIF_COL = {-1, -1, -1, 0, 0, 0, 1, 1, 1};

	public Compactus(Colonia colonia) {
		super(colonia);
	}
	
	@Override 
	public char getChar() {
		return 'O';
	}
	
	@Override
	public void creix(Cultiu cultiu) {
		int novaFila, novaCol;
		for (int i=0; i<DIF_FILA.length; i++) {
			novaFila = getFila()+DIF_FILA[i];
			novaCol = getCol()+DIF_COL[i];
			new Compactus(getColonia()).posa(cultiu, novaFila, novaCol);
		}
		
	}
}
