package scrabble;

public class Casella implements Cloneable{
	
	private char lletra;
	private int multiLletra = 1;
	private int multiParaula = 1;
	
	public Casella(char lletra, int multiLletra, int multiParaula) {
		this.lletra = lletra;
		this.multiLletra = multiLletra;
		this.multiParaula = multiParaula;
	}

	public char getLletra() {
		return lletra;
	}

	public void setLletra(char lletra) {
		this.lletra = lletra;
	}

	public int getMultiLletra() {
		return multiLletra;
	}

	public void setMultiLletra(int multiLletra) {
		this.multiLletra = multiLletra;
	}

	public int getMultiParaula() {
		return multiParaula;
	}

	public void setMultiParaula(int multiParaula) {
		this.multiParaula = multiParaula;
	}

	@Override
	public Casella clone() {
		try {
			return (Casella) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}

}
