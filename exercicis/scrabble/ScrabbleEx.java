package scrabble;

public class ScrabbleEx implements Comparable<ScrabbleEx>{
	
	private Casella[] caselles;

	public ScrabbleEx(Casella[] caselles) {
		this.caselles = caselles;
	}
	
	public int valor() {
		
		int valorLletra = 0;
		int valorParaula = 0;
		
		for(int i = 0; i < caselles.length; i++) {
						
			switch(caselles[i].getLletra())
			{
			case 'D': 
			case 'G':
				valorLletra = 2;
				break;
			case 'B':
			case 'C':
			case 'M':
			case 'P':
				valorLletra = 3;
				break;
			case 'F':
			case 'H':
			case 'V':
			case 'W':
			case 'Y':
				valorLletra = 4;
				break;
			case 'K':
				valorLletra = 5;
				break;
			case 'J':
			case 'X':
				valorLletra = 8;
				break;
			case 'Q':
			case 'Z':
				valorLletra = 10;
				break;
			default:
				valorLletra = 1;
				break;			
			}
			
			// aplica el modificador al valor de la lletra
			valorParaula += valorLletra * caselles[i].getMultiLletra();
		}
		// aplica el modificador al valor de la paraula
		for(int i = 0; i < caselles.length; i++)
			valorParaula *= caselles[i].getMultiParaula();
		
		return valorParaula;
	}

	@Override
	public int compareTo(ScrabbleEx altreScrabble) {
		return valor() - altreScrabble.valor();
	}
}
