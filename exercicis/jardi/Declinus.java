package jardi;

public class Declinus extends Planta {
	
	private boolean pucCanviar = false;
	private boolean maxAltura = false;
	
	public Llavor creix() {
		
		// la planta ha arribat a la seva al�ada m�xima per primer cop?
		if(!maxAltura && altura >= 4)
			maxAltura = true;
		// pot canviar l'al�ada, una vegada de cada dos
		if(pucCanviar) {
			// suma si ha de cr�ixer per� resta si ha de decr�ixer
			altura += maxAltura ? -1 : 1;
			pucCanviar = false;
		}
		else {
			pucCanviar = true;
		}
		// mor quan l'al�ada arriba a 0
		if(maxAltura && altura == 0)
			esViva = false;
		// retorna una llavor si l'al�ada arriba a 4
		return altura > 3 ? new Llavor(new Declinus()) : null;
	}
	
	public char getChar(int nivell) {
		
		// flor
		if(nivell == altura) {
			return '*';
		}
		// tija
		else if(nivell < altura) {
			return ':';
		}
		// buit
		else {
			return super.getChar(nivell);
		}
	}
}
