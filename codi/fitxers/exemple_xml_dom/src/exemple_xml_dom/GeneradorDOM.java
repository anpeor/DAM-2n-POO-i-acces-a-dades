package exemple_xml_dom;

import java.io.FileWriter;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class GeneradorDOM {
	public static void main(String[] args) {
		try (FileWriter writer = new FileWriter("empleats.xml")) {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = builder.newDocument();
			document.setXmlVersion("1.0");
			
			Element elementEmpleats = document.createElement("empleats");
			document.appendChild(elementEmpleats);

			Element elementEmpleat = document.createElement("empleat");
			elementEmpleat.setAttribute("id", "1");
			document.getDocumentElement().appendChild(elementEmpleat);

			Element elementCognom = document.createElement("cognom");
			Text text = document.createTextNode("Gasol");
			elementCognom.appendChild(text);
			elementEmpleat.appendChild(elementCognom);

			Source source = new DOMSource(document);
			Result result = new StreamResult(writer);

			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "5");
			transformer.transform(source, result);

			Result console = new StreamResult(System.out);
			transformer.transform(source, console);
		} catch (ParserConfigurationException | TransformerException | IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
