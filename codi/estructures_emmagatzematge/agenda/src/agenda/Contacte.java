package agenda;

import java.text.Collator;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

public class Contacte implements Comparable<Contacte> {
	private static final Contacte CONTACTE_BUIT = new Contacte("", "", "", "");
	private final String nom;
	private final String cognoms;
	private String adreca;
	private String telefon;
	
	public static final Comparator<Contacte> COMPARA_PER_NOM =
		(contacte, patro) -> contacte.getNom().compareTo(patro.getNom());
	public static final Comparator<Contacte> COMPARA_PER_COGNOMS =
		(contacte, patro) -> contacte.getCognoms().compareTo(patro.getCognoms());
	public static final Comparator<Contacte> COMPARA_PER_ADRECA =
		(contacte, patro) -> contacte.getAdreca().compareTo(patro.getAdreca());
	public static final Comparator<Contacte> COMPARA_PER_TELEFON =
		(contacte, patro) -> contacte.getTelefon().compareTo(patro.getTelefon());
	
	public Contacte(String nom, String cognoms, String adreca, String telefon) {
		this.nom = nom;
		this.cognoms = cognoms;
		setAdreca(adreca);
		setTelefon(telefon);
	}
	
	public String getNom() {
		return nom;
	}
	public String getCognoms() {
		return cognoms;
	}
	public String getAdreca() {
		return adreca;
	}
	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	@Override
	public int compareTo(Contacte arg0) {
		/*
		int res = cognoms.compareTo(arg0.cognoms);
		if (res == 0)
			res = nom.compareTo(arg0.nom);
		return res;
		*/
		Collator ordre = Collator.getInstance();
		int resultat = ordre.compare(cognoms, arg0.cognoms);
		
		if (resultat == 0)
			resultat = ordre.compare(nom, arg0.nom);
		
		return resultat;
	}
	
	public void cercaIElimina(List<Contacte> resultat, Comparator<Contacte> comparador) {
		Contacte c;
		ListIterator<Contacte> it;
		
		if (comparador.compare(this, CONTACTE_BUIT)!=0) {
			it = resultat.listIterator();
			while (it.hasNext()) {
				c = it.next();
				if (comparador.compare(this, c)!=0)
					it.remove();
			}
		}
	}
}
