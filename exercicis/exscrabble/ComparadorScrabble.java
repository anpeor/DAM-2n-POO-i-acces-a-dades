package scrabble;

import java.util.Comparator;

public class ComparadorScrabble implements Comparator<String>{
	
	public static Comparator<String> COMPARADOR_SCRABBLE = null;
	
	private ComparadorScrabble() {}

	private int valor(String paraula) {
		
		int valor = 0;
		
		for(int index = 0; index < paraula.length(); index++) {
			
			switch(paraula.charAt(index))
			{
			case 'D': 
			case 'G':
				valor += 2;
				break;
			case 'B':
			case 'C':
			case 'M':
			case 'P':
				valor += 3;
				break;
			case 'F':
			case 'H':
			case 'V':
			case 'W':
			case 'Y':
				valor += 4;
				break;
			case 'K':
				valor += 5;
				break;
			case 'J':
			case 'X':
				valor += 8;
				break;
			case 'Q':
			case 'Z':
				valor += 10;
				break;
			default:
				valor += 1;
				break;			
			}
		}
		
		return valor;
	}
	
	public static Comparator<String> getInstance() {
		
		if(COMPARADOR_SCRABBLE == null) {
			COMPARADOR_SCRABBLE = new ComparadorScrabble();
		}		
		return COMPARADOR_SCRABBLE;
	}

	@Override
	public int compare(String paraula1, String paraula2) {
		
		return valor(paraula1) - valor(paraula2);
	}
}
