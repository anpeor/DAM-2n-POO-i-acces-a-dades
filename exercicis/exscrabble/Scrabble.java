package scrabble;

public class Scrabble implements Comparable<Scrabble>{
	
	private String paraula = new String();
	
	public Scrabble(String paraula) throws IllegalArgumentException {
		
		// comprova cadascun dels car�cters de la paraula
		int index = 0;
		while(index < paraula.length() && Character.isLetter(paraula.charAt(index))) {
			index++;
		}
		// tots els car�cters de la paraula s�n lletres?
		if(index >= paraula.length()) {
			// s�, guarda la paraula
			this.paraula = paraula;
		}
		else {
			//no, llen�a una excepci�
			throw new IllegalArgumentException();
		}
	}
	
	public int valor() {
		
		int valor = 0;
		
		for(int index = 0; index < paraula.length(); index++) {
			
			switch(paraula.charAt(index))
			{
			case 'A': 
			case 'E':
			case 'I':
			case 'L':
			case 'N':
			case 'O':
			case 'R':
			case 'S':
			case 'T':
			case 'U':
				valor += 1;
				break;
			case 'D': 
			case 'G':
				valor += 2;
				break;
			case 'B':
			case 'C':
			case 'M':
			case 'P':
				valor += 3;
				break;
			case 'F':
			case 'H':
			case 'V':
			case 'W':
			case 'Y':
				valor += 4;
				break;
			case 'K':
				valor += 5;
				break;
			case 'J':
			case 'X':
				valor += 8;
				break;
			case 'Q':
			case 'Z':
				valor += 10;
				break;
			default:
				valor += 1;
				break;			
			}
		}
		
		return valor;
	}

	@Override
	public int compareTo(Scrabble paraulaAdversari) {
		return valor() - paraulaAdversari.valor();
	}
	
	@Override
	public String toString() {
		return paraula + ' ' + valor();
	}
}
