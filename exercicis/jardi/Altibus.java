package jardi;

public class Altibus extends Planta {
	
	public Llavor creix() {
		
		super.creix();
		return altura > 7 ? new Llavor(new Altibus()) : null; 
	}
	
	public char getChar(int nivell) {
		
		// flor
		if(nivell == altura) {
			return 'O';
		}
		// tija
		else if(nivell < altura) {
			return '|';
		}
		// buit
		else {
			return super.getChar(nivell);
		}	
	}
}
