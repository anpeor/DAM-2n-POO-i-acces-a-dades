package jardi;

public class Jardi {
	
	private Planta jardi[];
	
	public Jardi(int mida) {
		// mida del jardi
		jardi = new Planta[mida <= 0 ? 10 : mida];
		// testos a null
		for (int i = 0; i < mida; i++) {
			jardi[i] = null;
		}
	}
	
	public void temps() {
		
		for(int p = 0; p < jardi.length; p++) {
			// hi ha una planta en aquest test?
			if(jardi[p] != null) {
				// est� viva aquesta planta?
				if(jardi[p].esViva()) {
					
					Planta plantaRetornada = jardi[p].creix();
					// al cr�ixer la planta ha retornat alguna llavor?
					if(plantaRetornada instanceof Llavor) {
						// planta la nova llavor en un test buit
						int distancia = jardi[p].escampaLlavor();
						plantaLlavor(plantaRetornada, distancia + p);
						
					}
					// la llavor a germinat?
					else if(plantaRetornada != null){
						jardi[p] = plantaRetornada; 
					}				
				}
				// esborra plantes mortes
				else {
					jardi[p] = null;
				}
			}			
		}
	}
	
	@Override
	public String toString() {
		
		StringBuilder resultat = new StringBuilder();
		
		for(int nivell = 10; nivell >= 0; nivell--) {
			
			for(int pos = 0; pos < jardi.length; pos++) {
				
				if (jardi[pos] != null) 
					resultat.append(jardi[pos].getChar(nivell));
				else
					resultat.append(' ');
					
			}
			
			resultat.append('\n');
		}
		// terra
		for(int i = 0; i < jardi.length; i++) resultat.append('_');
		// separaci� entre etapes
		resultat.append('\n');
		
		return resultat.toString();
	}
	
	public boolean plantaLlavor(Planta novaPlanta, int pos) {
		// la llavor ha anat a parar fora del jardi o a sobre d'una altra planta?
		if(pos >= jardi.length || pos < 0 || jardi[pos] != null) {
			return false;
		}
		else {
			jardi[pos] = novaPlanta;
			return true;
		}
	}
}
