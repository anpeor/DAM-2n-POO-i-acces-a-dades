package jardi;

public class Llavor extends Planta {

	private Planta planta;
	private int temps = 0;
	
	public Llavor(Planta planta) {
		
		if (planta instanceof Llavor)
			throw new IllegalArgumentException();
		else
			this.planta = planta;
	}
	
	public Planta creix() {
		
		temps++;
		
		return temps >= 5 ? planta : null;
	}
	
	public char getChar(int nivell) {
		
		return nivell == 0 ? '.' : super.getChar(nivell);
	}
}
