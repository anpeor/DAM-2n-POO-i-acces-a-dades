## Entorn gràfic i bases de dades

En aquesta secció analitzarem alguns dels problemes de disseny habituals amb
què ens trobem quan volem escalar una aplicació amb JDBC.

Utilitzarem de base una aplicació força bàsica que ens permet, mitjançant
una finestra gràfica, mantenir la informació sobre les pel·lícules que
guardem a la base de dades Sakila: modificar pel·lícules existents, crear-ne
de noves, o esborrar-les.

L'aplicació és només un esborrany: no hi ha un bon control d'errors i no es
poden modificar tots els camps de les pel·lícules, però en tenim suficient
per veure com podríem enfocar les coses per començar a construir una
aplicació més seriosa amb JDBC i JavaFX.

El codi complet es pot veure [aquí](codi/jdbc/filmfx/src). Per fer-la
funcionar cal utilitzar el connector de MySQL, ja que el connector de
MariaDB no disposa, en el moment d'escriure això, de totes les funcionalitats
que volem utilitzar.

L'aplicació està organitzada utilitzant el patró MVC, així que podem
analitzar cada part per separat.

### Model

#### *Film*

La classe principal del model és *Film*. *Film* guarda les dades corresponents
a una de les pel·lícules i és bàsicament un continent per aquesta
informació.

Podem veure que la classe *Film* és una classe **POJO** (*Plain Old
Java Object*), és a dir, que no es veu modificada pel fet que les dades
de les pel·lícules vinguin d'una base de dades, sinó que és una classe Java
absolutament normal.

Sí que crida l'atenció que tenim alguns atributs que no apareixerien en un
disseny purament orientat a objectes, com el *filmId* o el *languageId*.

### Controlador

El controlador s'encarrega de traduir el model relacional al model
orientat a objectes.

Hi tenim dues classes: *ConnectionProperties* i *FilmController*.

#### *ConnectionProperties*

*ConnectionProperties* simplement conté les dades necessàries per establir
una connexió amb la base de dades. És important que aquestes dades estiguin
en un fitxer a part de la resta del codi. Idealment estarien en un fitxer de
configuració per evitar haver de recompilar el programa si es modifiquen.

En qualsevol cas, és important que aquest fitxer no acabi mai en un
repositori de *git* públic, per tal d'evitar revelar les dades de connexió.
Podríem, per exemple, posar un fitxer amb dades comodí, i després afegir el
nom del fitxer al *.gitignore* per evitar que qualsevol modificació pugi
al repositori inadvertidament.

#### *FilmController*

Aquesta és una de les classes més importants del programa. S'encarrega de
traduir la taula *films* del model relacional a la classe *Film*.

Utilitza un *JdbcRowSet* per a interaccionar amb la base de dades.
*JdbcRowSet* és una interfíce que amplia la interfície *ResultSet* que ja
coneixem.

Un *JdbcRowSet* manté sempre una connexió amb la base de dades i ens permet
navegar pels resultats d'una consulta en qualsevol moment. A més,
ens permet modificar els valors que hi ha a la base de dades.

El constructor de *FilmController* crea el *JdbcRowSet* i el vincula a la
taula *Film* de la base de dades:

```java
RowSetFactory rowSetFactory = RowSetProvider.newFactory();
rowSet = rowSetFactory.createJdbcRowSet();
rowSet.setUrl(ConnectionProperties.DB_URL);
rowSet.setUsername(ConnectionProperties.DB_USER);
rowSet.setPassword(ConnectionProperties.DB_PASS);
rowSet.setCommand("SELECT * FROM film");
rowSet.execute();
```

Els mètodes auxiliars *updateRowSet()* i *updateFilm()* són els responsables
de traduir les dades de la taula als atributs d'un objecte i a l'inversa.

```java
private void updateRowSet(Film f) throws SQLException {
  rowSet.updateInt("film_id", f.getFilmId());
  rowSet.updateString("title", f.getTitle());
...
```

```java
private void updateFilm(Film f) throws SQLException {
  f.setFilmId(rowSet.getInt("film_id"));
  f.setTitle(rowSet.getString("title"));
...
```

Això implica diverses decisions de disseny:

- La traducció del nom dels camps (a BD s'utilitza *snake case* i en OO
s'utilitza *camel case*).

- La traducció dels tipus entre el SGBD i Java. Alguns són senzills, com
decidir que un *varchar* passa a un *String*, però d'altres no tant:

    Com tractem un tipus *year* en Java?

    Com assegurem que un *number(4,2)*, que
sempre té exactament dos decimals es comporta bé en un *float*? O potser
és millor multiplicar-lo per 100, tractar-lo com un *int*, i dividir-lo per
100 un altre cop al guardar-lo a la base de dades?

    Què passa amb un *enum*? El tractem com a cadena, o creem un *enum*
equivalent en Java?

- La gestió de les claus forànies. Recuperem automàticament el nom de
l'idioma associat a una pel·lícula i el guardem a *Film*? O creem una nova
classe anomenada *Language* per guardar la relació entre l'*id* i el nom de
l'idioma? Recuperem automàticament aquesta dada sempre que recuperem una
pel·lícula de la BD, o ho fem sota demanda només quan la necessitem?

Els altres mètodes de *FilmController* permeten navegar per la llista de
pel·lícules i fer les modificacions necessàries a la base de dades.

Per exemple, el mètode *getCurrent()* retorna un objecte *Film* amb les dades
de la fila actual del cursor:

```java
public Film getCurrent() {
  Film f = new Film();
  try {
    rowSet.moveToCurrentRow();
    updateFilm(f);
  } catch (SQLException ex) {
    ex.printStackTrace();
  }
  return f;
}
```

I el mètode *update()* actualitza una pel·lícula existent amb les dades que
es reben a través d'un objecte *Film*:

```java
public Film update(Film f) {
  try {
    updateRowSet(f);
    rowSet.updateRow();
    rowSet.moveToCurrentRow();
  } catch (SQLException ex) {
    try {
      rowSet.rollback();
    } catch (SQLException e) {

    }
    ex.printStackTrace();
  }
  return f;
}
```

### Vista

La vista s'encarrega d'interaccionar amb l'usuari: mostrar les dades de les
pel·lícules i rebre les ordres.

La vista mai no gestiona directament la informació, sinó que es comunica amb
la base de dades a través del controlador.

A la vista hi tenim dues classes: *FilmMain* i *FilmUI*.

#### *FilmMain*

*FilmMain* és pròpiament l'aplicació JavaFX, però delega tota la feina a
*FilmUI*:

```java
public class FilmMain extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setScene(new Scene(new FilmUI(), 800, 220));
		stage.show();
	}

}
```

#### *FilmUI*

*FilmUI* és el panell principal de l'aplicació (un *BorderPane* en aquest
cas). També rep les pulsacions dels botons de l'aplicació, però en delega
la seva resolució al controlador.

*FilmUI* conté camps de text per a tots els atributs d'una pel·lícula, així
com botons que permeten a l'usuari navegar per la llista de pel·lícules i
confirmar les modificacions que es facin.

Els mètodes auxiliars *getFieldData()* i *setFieldData()* són els encarregats
d'agafar les dades del formulari i crear un *Film* amb elles, o de poblar
el formulari amb les dades d'un *Film* existent:

```java
private Film getFieldData() {
  Film f = new Film();
  f.setFilmId(Integer.parseInt(idField.getText()));
  f.setTitle(titleField.getText());
...
```

```java
private void setFieldData(Film f) {
  idField.setText(""+f.getFilmId());
  titleField.setText(f.getTitle());
...
```

La classe interna *ButtonHandler* gestiona la pulsació de tots els botons
de l'aplicació.

Per exemple, el codi per modificar una pel·lícula és:

```java
Film f = getFieldData();
...
if (bean.update(f) != null)
  msgLabel.setText("Film with ID:"
      + String.valueOf(f.getFilmId() + " updated successfully"));
...
```

O el codi per esborrar una pel·lícula:

```java
f = bean.getCurrent();
bean.delete();
msgLabel.setText(
    "Film with ID:" + String.valueOf(f.getFilmId() + " deleted successfully"));
```

### Conclusions

Tot i l'aparent senzilla del problema (només volem mantenir una taula de totes
les que hi ha a la BD), ens trobem per una banda amb decisions de disseny
complexes i per altra banda amb una gran quantitat de codi repetitiu.

A les properes unitats veurem sistemes ORM, que ens permeten automatitzar
les parts monòtones del codi, i concentrar els esforços al disseny.
