package cerca_binaria.e;

import java.util.ArrayList;
import java.util.List;

public class LlistaOrdenada {
	// resolem aquests warning a l'exercici següent
	private List<Comparable> llista = new ArrayList<Comparable>();
	
	public int binarySearch(Comparable element) {
		int pos = -1;
		int inici = 0;
		int fi = llista.size() - 1;
		int mig;
		int comparacio;
		Comparable elementMig;
		while (fi - inici >= 0) {
			mig = (inici + fi) / 2;
			elementMig = llista.get(mig);
			comparacio = element.compareTo(elementMig);
			if (comparacio == 0) {
				pos = mig;
				inici = fi + 1;
			} else if (comparacio < 0) {
				fi = mig - 1;
			} else { // element > elementMig
				inici = mig + 1;
			}
		}
		
		return pos;
	}
	
	public void add(Comparable element) {
		/*
		 * No podem utilitzar directament el mètode binarySearch perquè en cas que
		 * ens torni -1 (l'element no hi és) no sabem la posició on posar-lo.
		 * Tornem a implementar la cerca binària amb una lleugera modificació
		 * perquè ens guardi a pos el lloc on ha d'anar l'element
		 */
		int pos = 0;
		int inici = 0;
		int fi = llista.size() - 1;
		int mig;
		Comparable elementMig;
		int comparacio;
		while (fi - inici >= 0) {
			mig = (inici + fi) / 2;
			elementMig = llista.get(mig);
			// resolem aquest warning a l'exercici 6
			comparacio = element.compareTo(elementMig);
			if (comparacio == 0) {
				inici = fi + 1;
				pos = mig;
			} else if (comparacio < 0) {
				fi = mig - 1;
				pos = mig;
			} else { // element > elementMig
				pos = inici = mig + 1;
			}
			
		}
		llista.add(pos, element);
	}
	
	public Comparable get(int index) {
		return llista.get(index);
	}
	
	public boolean remove(Comparable element) {
		int pos = binarySearch(element);
		if (pos == -1)
			return false;
		else {
			llista.remove(pos);
			return true;
		}
	}
	
	public int size() {
		return llista.size();
	}
}
