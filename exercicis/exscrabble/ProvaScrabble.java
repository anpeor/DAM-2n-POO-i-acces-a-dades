package scrabble;

import java.util.Arrays;
import java.util.Comparator;

public class ProvaScrabble {

	public static void main(String[] args) {
		
		// Primera part
		Scrabble[] vector1 = {
				new Scrabble("BARCELONA"),
				new Scrabble("MADRID"),
				new Scrabble("SEVILLA"),
				new Scrabble("CORDOBA"),
				new Scrabble("ZARAGOZA"),
				new Scrabble("VALENCIA"),
				new Scrabble("GIRONA"),
				new Scrabble("TARRAGONA"),
				new Scrabble("LLEIDA")
				};
		Arrays.sort(vector1, new Comparator<Scrabble>() {
			// el resultat de la comparació retorna un nombre enter
			public int compare(Scrabble ob1, Scrabble ob2) {
				return ob1.compareTo(ob2);
			}
		});
		for(int i = 0; i < vector1.length; i++)
			System.out.println(vector1[i].toString());
		
		
		// Segona part
		String[] vector2 = {
				"BARCELONA",
				"MADRID",
				"SEVILLA",
				"CORDOBA",
				"ZARAGOZA",
				"VALENCIA",
				"GIRONA",
				"TARRAGONA",
				"LLEIDA"
				};
		// objecte comparador
		Comparator<String> comparador = ComparadorScrabble.getInstance();
		// comparador com a argument del sort
		Arrays.sort(vector2, comparador);
		for(int i = 0; i < vector2.length; i++)
			System.out.println(vector2[i].toString());
	
		
		// Tercera part
		Casella[] paraula = {
				new Casella('J', 1, 1), 
				new Casella('A', 1, 1),
				new Casella('V', 1, 2),
				new Casella('A', 1, 1)
				}; 
		ScrabbleEx scrabbleEx = new ScrabbleEx(paraula);
		System.out.println(scrabbleEx.valor());
	}
}
